'use strict';

const addStrings = (str1, str2) => {
  var str = "";
  for (let i = 0; i < str1.length; i++) {
    if (typeof str2[i] == "undefined" || isNaN(str2[i])) {
      str += parseInt(str1[i]);
    }
    else{
    str += parseInt(str1[i]) + parseInt(str2[i]);
    }
  }
  return str;
}

const getSum = (str1, str2) => {
  if (typeof str1 != "string" && typeof str2 != "string") {return false}
  if (str1.length === 0){str1 = "0";}
  if (str2.length === 0){str2 = "0";}
  if (!(/^\d+$/.test(str1) && /^\d+$/.test(str2))) {return false}

  if (str1.length === str2.length) {return addStrings(str1, str2)}
  if (str1.length > str2.length) {return addStrings(str1, str2)}
  else{return addStrings(str2, str1)}
};

const getPosts = (listOfPosts, authorName) => {
  var count = 0;
  for (const i of listOfPosts) {
    if (i.author === authorName) {
      count++;
    }
  }
  return count;
}

const getComments = (listOfPosts, authorName) => {
  var count = 0;
  for (const i of listOfPosts) {
    if (i.hasOwnProperty("comments")) {
      for (const j of i.comments) {
        if (j.author === authorName) {
          count++;
        }
      }
    }
  }
  return count;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postCount = getPosts(listOfPosts, authorName);
  let commentCount = getComments(listOfPosts, authorName);
  return `Post:${postCount},comments:${commentCount}`;
};

const tickets=(people)=> {
  let cashRegister = {
    $25: 0,
    $50: 0,
    $100: 0,
    giveChange: function(cash) {
      this.$25 -= cash[0];
      this.$50 -= cash[1];
      this.$100 -= cash[2];
    },
    cashSum: function() {
      return $25 * 25 + $50 * 50 + $100 * 100;
    }
  };
  for (const i of people) {
    if (!billCalculator(cashRegister, i)) {return "NO"}
  }
  return "YES";
};

const billCalculator = (cashRegister, testValue) => {
  if (testValue > cashRegister.cashSum) {
    return false;
  }
  else if (testValue === 100) {
    if (cashRegister.$25 >= 1 && cashRegister.$50 >= 1) {
      cashRegister.giveChange([1, 1, -1]);
      return true;
    }
    else if(cashRegister.$25 >= 3){
      cashRegister.giveChange([3, 0, -1]);
    }
  }
  else if (testValue === 50) {
    if(cashRegister.$25 >= 1){
      cashRegister.giveChange([1, -1, 0]);
      return true;
    }
  }
  else if (testValue === 25) {
    cashRegister.giveChange([-1, 0, 0]);
    return true;
  }
  return false;
}

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
